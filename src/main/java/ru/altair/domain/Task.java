package ru.altair.domain;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Tim on 10.10.2015.
 */
@Entity
@Table(name="task")
public class Task {

    @Id
    String id;

    @Column(name="title")
    String title;

    @Column(name="description")
    String description;

    @Column(name="assigned_to")
    String assignedTo;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="assigned_date")
    Date assignedDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="due_date")
    Date dueDate;

    @Column(name="status")
    Integer status;

    public Task() {
    }

    public Task(Date assignedDate, String assignedTo, String description, Date dueDate, String id, Integer status, String title) {
        this.assignedDate = assignedDate;
        this.assignedTo = assignedTo;
        this.description = description;
        this.dueDate = dueDate;
        this.id = id;
        this.status = status;
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(String assignedTo) {
        this.assignedTo = assignedTo;
    }

    public Date getAssignedDate() {
        return assignedDate;
    }

    public void setAssignedDate(Date assignedDate) {
        this.assignedDate = assignedDate;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}

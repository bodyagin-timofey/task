package ru.altair.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Tim on 10.10.2015.
 */
@Entity
@Table(name="users")
public class User {

    @Id
    String id;

    @Column(name="email")
    String email;

    @Column(name="name")
    String name;

    @Column(name="password")
    String password;

    public User() {
    }

    public User(String id, String email, String password, String name) {
        this.email = email;
        this.id = id;
        this.name = name;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

package ru.altair.service;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.util.HashMap;

/**
 * Created by Tim on 10.10.2015.
 */
public class Utils {

    private static Configuration config;

    public static String getBaseAddress(){
        HttpServletRequest req = Utils.getRequest();
        String url = req.getRequestURL().toString().replace(req.getServletPath(), "");
        url = url.replace("www.", "");
        return url;
    }

    public static String getDomain(HttpServletRequest req){
        String domain = req.getServerName();
        if(domain.startsWith("www.")){
            domain = domain.replace("www.", "");
        }
        domain = domain.substring(0, domain.lastIndexOf("."));
        if(!domain.contains(".")){
            return "";
        }
        domain = domain.substring(0, domain.lastIndexOf("."));
        return domain;
    }

    public static String getServerName(HttpServletRequest req){
        String serverName = req.getServerName();
        if(serverName.startsWith("www.")){
            serverName = serverName.replace("www.", "");
        }
        String subDomain = getDomain(req);
        if("".equals(subDomain)){
            return serverName;
        }
        return serverName.replace(subDomain, "").substring(1);
    }

    public static String readFile(String name){
        Resource resource = new ClassPathResource(name);
        BufferedReader br = null;
        try {
            br = new BufferedReader(new InputStreamReader(resource.getInputStream()),1024);
            StringBuilder stringBuilder = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                stringBuilder.append(line).append('\n');
            }
            br.close();
            return stringBuilder.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static void sendEmail(String address, String subj, String msg){
        JavaMailSenderImpl sender = new JavaMailSenderImpl();
        sender.setHost("localhost");
        MimeMessage message = sender.createMimeMessage();
        MimeMessageHelper helper = null;
        try {
            helper = new MimeMessageHelper(message, true);
            helper.setFrom("tim@anubias-narod.com");
//            helper.setSubject("Email verification");
            helper.setTo(address);
            helper.setSubject(subj);
            helper.setText(msg, true);
            sender.send(message);
        } catch (MessagingException e) {
            e.printStackTrace();
        }catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void sendEmail(String address, String subj, String templatePath, HashMap<String, String> values){
        if(config==null){
            config = new Configuration(Configuration.DEFAULT_INCOMPATIBLE_IMPROVEMENTS);
            config.setTemplateExceptionHandler(TemplateExceptionHandler.IGNORE_HANDLER);
        }
        Template template = null;
        try {
            template = new Template("t1", new StringReader(Utils.readFile(templatePath)), config);
            StringWriter sw = new StringWriter();
            template.process(values, sw);
            Utils.sendEmail(address, subj, sw.toString());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TemplateException e) {
            e.printStackTrace();
        }
    }



    public static HttpServletRequest getRequest(){
        return ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
    }
}

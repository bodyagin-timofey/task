package ru.altair.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;
import ru.altair.dao.UserDao;
import ru.altair.domain.User;

import java.util.List;
import java.util.UUID;

/**
 * Created by Tim on 10.10.2015.
 */
@Service
public class UserService {

    @Autowired
    UserDao userDao;

    public List<User> getAll(String domain){
        return userDao.getAll(domain);
    }

    public boolean login(String domain, String name, String pwd){
        User user = userDao.getByEmail(domain, name);
        if(user!=null && BCrypt.checkpw(pwd, user.getPassword()))
            return true;
        return false;
    }

    public String register(String domain, String name, String email, String password, String password2){
        String msg = "";
        if(name==null || name.isEmpty()){
            msg = "Name can not be empty <br>";
        }
        if(email==null || email.isEmpty()){
            msg = msg+"Email can not be empty <br>";
        }
        if(password==null || password.isEmpty() || password2==null || password2.isEmpty()){
            msg += "Password can not be empty <br>";
        } else {
            if(!password.equals(password2)){
                msg += "Passwords are not equal <br>";
            }
        }

        User user = userDao.getByEmail(domain, email);
        if(user!=null){
            msg += "User with email "+email+" is already registered <br>";
        }

        if(msg.isEmpty()){
            String id = UUID.randomUUID().toString();
            password = BCrypt.hashpw(password, BCrypt.gensalt());
            user = new User(id, email, password, name);
            userDao.save(domain, user);
        }

        return msg;
    }
}

package ru.altair.service;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;
import ru.altair.dao.CustomerDao;
import ru.altair.domain.Customer;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/**
 * Created by Tim on 10.10.2015.
 */
@Service
public class CustomerService {

    private final static List<String> disabledDomains = Arrays.asList(new String[]{"www", "task"});

    @Autowired
    CustomerDao customerDao;

    public boolean login(String name, String pwd){
        Customer customer = getByEmail(name);

        if(customer!=null && BCrypt.checkpw(pwd, customer.getPassword()))
            return true;
        return false;
    }

    public String register(String name, String email, String domain, String password, String password2){
        String msg = "";
        if(name==null || name.isEmpty()){
            msg = "Name can not be empty <br>";
        }
        if(email==null || email.isEmpty()){
            msg = msg+"Email can not be empty <br>";
        }
        if(domain==null || domain.isEmpty()){
            msg = msg+"Site can not be empty <br>";
        } else {
            if(disabledDomains.contains(domain)){
                msg = msg+"Can not register site with name "+domain+" <br>";
            }
            if(Context.getDomains().contains(domain)){
                msg = msg+"Site with name "+domain+" is already registered <br>";
            }
            Customer customer = customerDao.getByEmail(email);
            if(customer!=null){
                msg = msg+"Customer with email "+email+" is already registered <br>";
            }
            String str = domain.replaceAll("[a-z0-9-]","");
            if(!str.isEmpty()){
                msg = msg+"Site name can contains only letters, digits and '-' <br>";
            }
            if(domain.matches("[0-9-].*")){
                msg = msg+"Site name can start with letter only <br>";
            }
        }
        if(password==null || password.isEmpty() || password2==null || password2.isEmpty()){
            msg = msg+"Password can not be empty <br>";
        } else {
            if(!password.equals(password2)){
                msg = msg+"Passwords are not equal <br>";
            }
        }

        if(msg.isEmpty()){
            String id = UUID.randomUUID().toString();
            Customer customer = new Customer(id, email, password, name, domain);
            register(customer);
        }

        return msg;
    }

    private void register(Customer customer){
        customer.setPassword(BCrypt.hashpw(customer.getPassword(), BCrypt.gensalt()));
        customer.setVerificationCode(UUID.randomUUID().toString());
        customerDao.save(customer);
        Context.getDomains().add(customer.getDomain());
        sendVerifyEmail(customer);
    }

    public List<Customer> getAll(){
        return customerDao.getAll();
    }

    public Customer getByEmail(String email){
        return customerDao.getByEmail(email);
    }

    public void sendVerifyEmail(Customer customer){
        String url = Utils.getBaseAddress();
        String urlFull = url +"/verify?email="+ customer.getEmail()+"&code="+customer.getVerificationCode();

        Configuration config = new Configuration(Configuration.DEFAULT_INCOMPATIBLE_IMPROVEMENTS);
        config.setTemplateExceptionHandler(TemplateExceptionHandler.IGNORE_HANDLER);

        HashMap<String, String> values = new HashMap<String, String>();
        values.put("link_short", url);
        values.put("link_full", urlFull);
        values.put("code", customer.getVerificationCode());

        Utils.sendEmail(customer.getEmail(), "[GageMetrix] Confirm your email address", "mail_verification.html", values);
    }

    public void saveVerification(Customer customer){
        customerDao.initCustomerDatabase(customer.getDomain());
        customer.setVerificationCode(null);
        customerDao.save(customer);

        String urlCustomer = Utils.getBaseAddress();
        String url = urlCustomer.replace("://", "://" + customer.getDomain() + ".");
        HashMap<String, String> values = new HashMap<String, String>();
        values.put("link", url);
        values.put("link_customer", urlCustomer);
        Utils.sendEmail(customer.getEmail(), "Thank you for joining GageMetrix.com", "mail_verification_complete.html", values);
    }
}

package ru.altair.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.altair.dao.TaskDao;
import ru.altair.dao.UserDao;
import ru.altair.domain.Task;
import ru.altair.domain.User;

import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by Tim on 10.10.2015.
 */
@Service
public class TaskService {

    @Autowired
    TaskDao taskDao;

    public List<Task> getAll(String domain){
        return taskDao.getAll(domain);
    }

    public Task get(String domain, String id){
        return taskDao.get(domain, id);
    }

    public void save(String domain, Task task){
        if(task.getId()==null){
            task.setId(UUID.randomUUID().toString());
        }
        if(task.getAssignedDate()==null){
            task.setAssignedDate(new Date());
        }
        if(task.getStatus()==null){
            task.setStatus(1);
        }
        taskDao.save(domain, task);
    }
}

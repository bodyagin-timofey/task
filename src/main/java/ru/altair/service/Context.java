package ru.altair.service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by Tim on 10.10.2015.
 */
public class Context {

    private final static List<String> domains = new CopyOnWriteArrayList<String>();

    public static List<String> getDomains() {
        return domains;
    }
}

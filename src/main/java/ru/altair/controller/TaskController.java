package ru.altair.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import ru.altair.domain.Customer;
import ru.altair.domain.Task;
import ru.altair.domain.User;
import ru.altair.service.*;

import javax.annotation.PostConstruct;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by Tim on 10.10.2015.
 */
@Controller
public class TaskController {

    @PostConstruct
    private void init() {
        Context.getDomains().clear();
        List<Customer> customerList = customerService.getAll();
        for (Customer customer : customerList) {
            Context.getDomains().add(customer.getDomain());
        }
    }

    @Autowired
    CustomerService customerService;

    @Autowired
    UserService userService;

    @Autowired
    TaskService taskService;

    @RequestMapping(value = "/", method = {RequestMethod.GET})
    String rootPage(ModelMap model, HttpServletRequest req) {
        String domain = Utils.getDomain(req);
        if (domain.isEmpty()) {
            String email = getPrincipal();
            if (email != null) {
                Customer customer = customerService.getByEmail(email);
                if (customer != null) {
                    model.addAttribute("customer_name", customer.getName());
                    model.addAttribute("customer_email", customer.getEmail());
                    model.addAttribute("customer_domain", customer.getDomain());

                    if (customer.getVerificationCode() == null) { // email is verified
                        List<User> users = userService.getAll(customer.getDomain());
                        List<String> userNames = new ArrayList<String>();
                        for (User user : users) {
                            userNames.add(user.getName());
                        }
                        model.addAttribute("users", userNames);
                        String url = req.getRequestURL().toString();
                        url = url.replace("www.", "");
                        url = url.replace("://", "://" + customer.getDomain() + ".");
                        model.addAttribute("url", url);
                        return "customerPage";
                    } else {
                        return "verificationPage";
                    }
                }
            }
            return "login";
        } else {
            if (Context.getDomains().contains(domain)) {
                List<Task> tasks = taskService.getAll(domain);
                for (Task task : tasks) {
                    task.setDescription(task.getDescription().replace("\n", "<br>"));
                }
                model.addAttribute("tasks", tasks);
                return "taskPage";
            } else {
                return "notFound";
            }
        }
    }

    @RequestMapping(value = "/", method = {RequestMethod.POST})
    String saveTask(@ModelAttribute("task") Task task, ModelMap model, HttpServletRequest req) {
        String domain = Utils.getDomain(req);
        if (!domain.isEmpty() && Context.getDomains().contains(domain)) {
            taskService.save(domain, task);
            List<Task> tasks = taskService.getAll(domain);
            model.addAttribute("tasks", tasks);
            return "redirect:/";
        } else {
            return "notFound";
        }
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    String loginPage(ModelMap model,
                     @RequestParam(value = "error", required = false) String error,
                     @RequestParam(value = "logout", required = false) String logout,
                     HttpServletRequest req) {
        if (error != null) {
            model.addAttribute("msg", "Invalid username and password!");
        }
        if (logout != null) {
            req.getSession().invalidate();
            return "redirect:/";
        }

        return "login";
    }


    @RequestMapping(value = "/register", method = RequestMethod.GET)
    String registerPage(ModelMap model, HttpServletRequest req) {
        String domain = Utils.getDomain(req);
        if (domain.isEmpty()) {
            model.addAttribute("server", Utils.getServerName(req));
            return "registerCustomerPage";
        } else {
            if (Context.getDomains().contains(domain)) {
                model.addAttribute("domain", domain);
                return "registerPage";
            } else {
                return "notFound";
            }
        }
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    String registerSave(@ModelAttribute("customer") Customer customer, @ModelAttribute("password2") String password2, ModelMap model, HttpServletRequest req) {

        String msg = "";
        String domainReq = Utils.getDomain(req);
        if (domainReq.isEmpty()) {
            msg = customerService.register(customer.getName(), customer.getEmail(), customer.getDomain().toLowerCase(), customer.getPassword(), customer.getPassword());
        } else {
            if (Context.getDomains().contains(domainReq)) {
                msg = userService.register(domainReq, customer.getName(), customer.getEmail(), customer.getPassword(), password2);
            } else {
                return "notFound";
            }
        }
        if (msg.isEmpty()) {
            model.addAttribute("login", customer.getEmail());
            model.addAttribute("password", customer.getPassword());
            return "login";
        } else {
            model.addAttribute("msg", msg);
            if (!domainReq.isEmpty()) {
                model.addAttribute("domain", domainReq);
                return "registerPage";
            } else {
                model.addAttribute("server", Utils.getServerName(req));
                return "registerCustomerPage";
            }

        }
    }

    String verifySave(String email, String code) {
        if (email != null) {
            Customer customer = customerService.getByEmail(email);
            if (customer != null) {
                if (customer.getVerificationCode() == null || customer.getVerificationCode().equals(code)) {
                    if (customer.getVerificationCode() != null) {
                        customerService.saveVerification(customer);
                    }
                    return "redirect:/";
                } else {
                    return "verificationPage";
                }
            }
        }
        return "notFound";
    }

    @RequestMapping(value = "/verify", method = RequestMethod.POST)
    String verifyPost(@ModelAttribute("code") String code, ModelMap model, HttpServletRequest req) {
        String domainReq = Utils.getDomain(req);
        if (domainReq.isEmpty()) {
            String email = getPrincipal();
            if (email != null) {
                String path = verifySave(email, code);
                model.clear();
                if(path=="verificationPage"){
                    model.addAttribute("msg", "Invalid verification code");
                }
                return path;
            }
        } else {
            return "notFound";
        }
        return "notFound";
    }

    // example http://task.com:8080/task/verify?email=d@d.d&52578ca7-65ce-476f-abb7-1e47d63b3b05
    @RequestMapping(value = "/verify", method = RequestMethod.GET)
    String verifyGet(ModelMap model,
                     @RequestParam(value = "email", required = false) String email,
                     @RequestParam(value = "code", required = false) String code,
                     HttpServletRequest req) {
        String domainReq = Utils.getDomain(req);
        if (domainReq.isEmpty()) {
            if (email != null) {
                String path = verifySave(email, code);
                model.clear();
                if(path=="verificationPage"){
                    model.addAttribute("msg", "Invalid verification code");

                } else {
                    model.addAttribute("msg", "Thank you for verifying email. Please sign up.");
                    model.addAttribute("msg_type", "alert-success");
                }
                model.addAttribute("login", email);
                return "login";
            }
        } else {
            return "notFound";
        }
        return "notFound";
    }

    @RequestMapping(value = "/Access_Denied", method = RequestMethod.GET)
    public String accessDeniedPage(ModelMap model) {
        model.addAttribute("user", getPrincipal());
        return "accessDenied";
    }

    private String getPrincipal() {
        String userName = null;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal instanceof UserDetails) {
            userName = ((UserDetails) principal).getUsername();
        } else {
            userName = principal.toString();
        }
        return userName;
    }

    private String getPrincipal(HttpServletRequest req) {
        String userName = null;
        SecurityContext context  = (SecurityContext) req.getSession(true).getAttribute("SPRING_SECURITY_CONTEXT");
        Object principal = context.getAuthentication().getPrincipal();

        if (principal instanceof UserDetails) {
            userName = ((UserDetails) principal).getUsername();
        } else {
            userName = principal.toString();
        }
        return userName;
    }

}

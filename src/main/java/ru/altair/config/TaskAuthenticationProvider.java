package ru.altair.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import ru.altair.service.Context;
import ru.altair.service.CustomerService;
import ru.altair.service.UserService;
import ru.altair.service.Utils;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tim on 10.10.2015.
 */
@Component
public class TaskAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    CustomerService customerService;

    @Autowired
    UserService userService;

    public Authentication authenticate(Authentication authentication)
            throws AuthenticationException {

        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        HttpServletRequest req = ((ServletRequestAttributes)RequestContextHolder.currentRequestAttributes()).getRequest();

        String name = authentication.getName();
        String password = authentication.getCredentials().toString();

        String domain = Utils.getDomain(req);

        boolean correct = false;
        if(domain.isEmpty()){
            correct = customerService.login(name, password);
        } else {
            if(Context.getDomains().contains(domain)){
                correct = userService.login(domain, name, password);
            }
        }

        if(correct){
            List<GrantedAuthority> grantedAuths = new ArrayList<GrantedAuthority>();
            return new UsernamePasswordAuthenticationToken(name, password, grantedAuths);
        } else {
            throw new BadCredentialsException("Incorrect login/password");
        }


    }

    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}
package ru.altair.dao;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.springframework.stereotype.Component;
import ru.altair.domain.Task;
import ru.altair.domain.User;
import ru.altair.service.HibernateUtil;

import java.util.List;

/**
 * Created by Tim on 10.10.2015.
 */
@Component
public class TaskDao {

    public List<Task> getAll(String domain){
        Session session = HibernateUtil.getSession(domain);
        List<Task> list = session.createCriteria(Task.class).addOrder(Order.desc("assignedDate")).list();
        session.close();
        return list;
    }

    public Task get(String domain, String id){
        Session session = HibernateUtil.getSession(domain);
        Task task = (Task)session.get(Task.class, id);
        session.close();
        return task;
    }

    public void save(String domain, Task task){
        Session session = HibernateUtil.getSession(domain);
        Transaction tx = session.beginTransaction();
        session.save(task);
        tx.commit();
        session.close();
    }
}

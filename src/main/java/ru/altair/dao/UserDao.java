package ru.altair.dao;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Component;
import ru.altair.domain.Customer;
import ru.altair.domain.User;
import ru.altair.service.HibernateUtil;

import java.util.List;

/**
 * Created by Tim on 10.10.2015.
 */
@Component
public class UserDao {

    public List<User> getAll(String domain){
        Session session = HibernateUtil.getSession(domain);
        List<User> list = session.createCriteria(User.class).list();
        session.close();
        return list;
    }

    public User getByEmail(String domain, String email){
        Session session = HibernateUtil.getSession(domain);
        Transaction tx = session.beginTransaction();
        Query query = session.createQuery("from User where email=:email");
        query.setString("email", email);
        List<User> users = query.list();
        session.close();
        if(users.size()>0){
            return users.get(0);
        }
        return null;
    }

    public void save(String domain, User user){
        Session session = HibernateUtil.getSession(domain);
        Transaction tx = session.beginTransaction();
        session.save(user);
        tx.commit();
        session.close();
    }
}

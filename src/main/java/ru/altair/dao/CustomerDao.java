package ru.altair.dao;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.SimpleExpression;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Component;
import ru.altair.domain.Customer;
import ru.altair.service.HibernateUtil;
import ru.altair.service.Utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;

/**
 * Created by Tim on 10.10.2015.
 */
@Component
public class CustomerDao {

    public List<Customer> getAll(){
        Session session = HibernateUtil.getSession("task");
        List<Customer> list = session.createCriteria(Customer.class).list();
        session.close();
        return list;
    }

    public Customer getByEmail(String email){
        Session session = HibernateUtil.getSession("task");
        List<Customer> list = session.createCriteria(Customer.class).add(Restrictions.eq("email", email)).list();
        session.close();
        if(list.size()>0){
            return list.get(0);
        }
        return null;
    }

    public void save(Customer customer){
        Session session = HibernateUtil.getSession("task");
        Transaction tx = session.beginTransaction();
        session.merge(customer);
        tx.commit();
        session.close();
    }

    public void initCustomerDatabase(String databaseName){
        Session session = HibernateUtil.getSession("task");
        Transaction tx = session.beginTransaction();
        Query query = session.createSQLQuery("create database "+databaseName);
        query.executeUpdate();
        tx.commit();
        session.close();

        session = HibernateUtil.getSession(databaseName);
        tx = session.beginTransaction();

        try {
            String q = Utils.readFile("init_database.sql");
            q = q.replace("\n", " ");
            String[] arr = q.split(";");
            for(String sql: arr){
                if(!sql.replace(" ", "").equals("")){
                    query = session.createSQLQuery(sql);
                    query.executeUpdate();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//        query = session.createSQLQuery("create table users (id varchar(36), email varchar(50), name varchar(255), password varchar(100))");
//        query.executeUpdate();
//        query = session.createSQLQuery("create table task (id varchar(36), title varchar(255), description varchar(4000), assigned_to varchar(255), assigned_date datetime, due_date datetime, status smallint)");
//        query.executeUpdate();
        tx.commit();
        session.close();
    }
}

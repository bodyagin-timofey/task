<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <%@ include file="head.html" %>
</head>
<body>

<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Multi-tenancy Proof of Concept</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href=".">Refresh</a></li>
                <li><a href='<c:url value="/login?logout"/>'>Logout</a></li>
            </ul>
        </div>
        <!--/.nav-collapse -->
    </div>
    <!--/.container-fluid -->
</nav>

<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Customer info:</h3>
        </div>
        <ul class="list-group">
            <li class="list-group-item list-group-item-lg">Customer: ${customer_name}</li>
            <li class="list-group-item list-group-item-lg">Email: ${customer_email}</li>
            <li class="list-group-item list-group-item-lg">Domain: ${customer_domain}</li>
            <li class="list-group-item list-group-item-lg">Link to tasks: <a href="${url}">${url}</a></li>
        </ul>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Registered users:</h3>
        </div>
        <ul class="list-group">
            <c:forEach var="user" items="${users}">
                <li class="list-group-item list-group-item-lg">${user}</li>
            </c:forEach>
        </ul>
    </div>
</div>
</body>
</html>

<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <%@ include file="head.html" %>
    <script src="js/script.js"></script>
</head>
<body>

<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Multi-tenancy Proof of Concept</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href=".">Refresh</a></li>
                <li><a href="javascript:showAddForm()">Add</a></li>
                <li><a href='<c:url value="/login?logout"/>'>Logout</a></li>
            </ul>
        </div>
        <!--/.nav-collapse -->
    </div>
    <!--/.container-fluid -->
</nav>

<div class="container">
    <div id="add-form-panel" class="panel panel-default" style="display:none;">
        <div class="panel-heading">
            <h3 class="panel-title">Add task</h3>
        </div>
        <div class="panel-body">
            <form method='POST' class="form-task">
                <label for="inputTitle">Title</label>
                <input type="text" id="inputTitle" name="title" class="form-control"
                       placeholder="Title" required autofocus>

                <label for="inputDescr">Description</label>
                    <textarea id="inputDescr" name="description" class="form-control" placeholder="Description"
                              required></textarea>

                <div class="row">
                    <div class="col-md-6">
                        <label for="inputAssigned">Assigned to</label>
                        <input type="text" id="inputAssigned" name="assignedTo"
                               class="form-control"
                               placeholder="Assigned to" required>
                    </div>
                    <div class="col-md-6" span="6">
                        <label for="inputDueDate">Due date</label>
                        <input type="text" id="inputDueDate" name="dueDate"
                               class="form-control"
                               placeholder="Due date" required>
                    </div>
                    <script type="text/javascript">
                            $(function () {
                                $('#inputDueDate').datepicker({});
                            });
                    </script>
                </div>
                <div class="row" style="margin-top:10px;">
                    <div class="col-md-6">
                        <button type="submit" class="btn btn-primary btn-save btn-block">Save</button>
                    </div>
                    <div class="col-md-6" span="6">
                        <button id="cancel-button" type="button" class="btn btn-default btn-cancel btn-block">Cancel
                        </button>
                    </div>
                </div>
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            </form>
        </div>
    </div>

    <c:forEach var="task" items="${tasks}">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">${task.title}</h3>
            </div>
            <div class="panel-body">
                ${task.description}
            </div>
            <div class="panel-footer">
                <div class="row">
                    <div class="col-md-4">
                        Assigned to: ${task.assignedTo}
                    </div>
                    <div class="col-md-4">
                        Assigned date: <fmt:formatDate type="both" value="${task.assignedDate}" pattern="MM/dd/yyyy HH:mm" />
                    </div>
                    <div class="col-md-4">
                        Due date: <fmt:formatDate type="both" value="${task.dueDate}" pattern="MM/dd/yyyy" />
                    </div>
                </div>
            </div>
        </div>
    </c:forEach>
</div>
</body>
</html>

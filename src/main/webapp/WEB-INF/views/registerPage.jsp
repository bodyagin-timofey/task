<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
    <%@ include file="head.html" %>
</head>
<body>
    <div class="container">
          <form class="form-signin" name="registerForm" action="<c:url value='register' />" method='POST'>
            <h3 class="form-signin-heading">Please register</h3>
            <c:if test="${not empty msg}">
                <div class="alert alert-danger" role="alert">
                  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                  ${msg}
                </div>
            </c:if>
            <label for="inputName">Name</label>
            <input type="text" id="inputName" name="name" value="${customer.name}" class="form-control" placeholder="Name" required autofocus>

            <label for="inputEmail">Email address</label>
            <input type="email" id="inputEmail" name="email" value="${customer.email}" class="form-control" placeholder="Email address" required>

            <c:if test="${empty domain}">
                <label for="inputDomain">Subdomain name</label>
                <input type="text" id="inputDomain" name="domain"  value="${customer.domain}" class="form-control" placeholder="Subdomain name" required>
            </c:if>

            <label for="inputPassword">Password</label>
            <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" required>

            <label for="inputPassword2">Repeat password</label>
            <input type="password" id="inputPassword2" name="password2" class="form-control" placeholder="Repeat password" required>

            <button class="btn btn-lg btn-primary btn-block" type="submit">Register</button>

            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
          </form>
    </div>
</body>
</html>

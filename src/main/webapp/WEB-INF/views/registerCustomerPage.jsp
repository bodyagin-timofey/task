<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
    <%@ include file="head.html" %>
    <script src="js/registerCustomer.js"></script>
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
</head>
<body>
<div class="container">
    <form class="form-signin" name="registerForm" action="<c:url value='register' />" method='POST'>
        <h3 class="form-signin-heading">Please register</h3>
        <c:if test="${not empty msg}">
            <div class="alert alert-danger" role="alert">
                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                ${msg}
            </div>
        </c:if>

        <span data-toggle="tooltip" class="badge pull-right" data-placement="bottom" title="Site can contains only letters, digits and '-'. Once your site is provisioned, the site address cannot be changed.">?</span>
        <label for="inputDomain">Claim your site </label>
        <div class="input-group">
            <input type="text" id="inputDomain" name="domain" value="${customer.domain}" class="form-control"
                   placeholder="Site name" required autofocus>
            <span class="input-group-addon">${server}</span>
        </div>

        <label for="inputName">Name</label>
        <input type="text" id="inputName" name="name" value="${customer.name}" class="form-control" placeholder="Name"
               required>

        <label for="inputEmail">Email address</label>
        <input type="email" id="inputEmail" name="email" value="${customer.email}" class="form-control"
               placeholder="Email address" required>

            <label class="checkbox-inline pull-right"><input id="show_password" type="checkbox" value="">Show password</label>
        <label for="inputPassword">Password</label>
        <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" required>

        <button class="btn btn-lg btn-primary btn-block" type="submit">Register</button>

        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    </form>
</div>
</body>
</html>

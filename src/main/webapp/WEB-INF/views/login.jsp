<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
    <%@ include file="head.html" %>
</head>
<body>

    <div class="container">
          <form class="form-signin" name="loginForm" action="<c:url value='login' />" method='POST'>
            <h3 class="form-signin-heading">Please sign in</h3>
            <c:if test="${not empty msg}">
                <c:if test="${empty msg_type}">
                    <c:set var="msg_type" value="alert-danger"/>
                </c:if>
                <div class="alert ${msg_type}" role="alert">
                  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                  ${msg}
                </div>
            </c:if>
            <label for="inputEmail" class="sr-only">Email address</label>
            <input type="email" id="inputEmail" name="username" value="${login}" class="form-control" placeholder="Email address" required autofocus>
            <label for="inputPassword" class="sr-only">Password</label>
            <input type="password" id="inputPassword" name="password" value="${password}" class="form-control" placeholder="Password" required>
            <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
            <a href="register">Register</a>
          </form>
    </div>
</body>
</html>